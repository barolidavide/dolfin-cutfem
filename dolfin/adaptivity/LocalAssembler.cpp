// Copyright (C) 2011 Marie E. Rognes
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg 2013
//
// First added:  2011-01-04
// Last changed: 2013-01-10

#include <dolfin/common/types.h>
#include <Eigen/Dense>

#include <dolfin/fem/UFC.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Facet.h>
#include "LocalAssembler.h"

using namespace dolfin;

//------------------------------------------------------------------------------
void
LocalAssembler::assemble(Eigen::MatrixXd& A,
                         UFC& ufc,
                         const std::vector<double>& coordinate_dofs,
                         ufc::cell& ufc_cell,
                         const Cell& cell,
                         const MeshFunction<std::size_t>* cell_domains,
                         const MeshFunction<std::size_t>* exterior_facet_domains,
                         const MeshFunction<std::size_t>* interior_facet_domains)
{
  // Clear tensor
  A.setZero();

  cell.get_cell_data(ufc_cell);

  // Assemble contributions from cell integral
  assemble_cell(A, ufc, coordinate_dofs, ufc_cell, cell, cell_domains);

 // std::cout << "after cell A=" << A << std::endl;

  // Assemble contributions from facet integrals
  if (ufc.form.has_exterior_facet_integrals()
	  || ufc.form.has_interior_facet_integrals())
  {
	for (FacetIterator facet(cell); !facet.end(); ++facet)
	{
	  ufc_cell.local_facet = facet.pos();
	  const int Ncells = facet->num_entities(cell.dim());
	  if (Ncells == 2)
	  {
		assemble_interior_facet(A, ufc, coordinate_dofs, ufc_cell, cell,
								*facet, facet.pos(), interior_facet_domains,
								cell_domains);
		//  std::cout << "after facet A=" << A << std::endl;
		 //std::cout << "A=" << A << std::endl;
	  }
	  else if (Ncells == 1)
	  {
		assemble_exterior_facet(A, ufc, coordinate_dofs, ufc_cell, cell,
								*facet, facet.pos(), exterior_facet_domains);
	  }
	  else
	  {
		dolfin_error("LocalAssembler.cpp",
					 "assemble local problem",
					 "Cell <-> facet connectivity not initialized, found "
					 "facet with %d connected cells. Expected 1 or 2 cells",
					 Ncells);
	  }
	}
  }

  // Check that there are no vertex integrals
  if (ufc.form.has_vertex_integrals())
  {
	dolfin_error("LocalAssembler.cpp",
				 "assemble local problem",
				 "Local problem contains vertex integrals which are not yet "
				 "supported by LocalAssembler");
  }
}
//------------------------------------------------------------------------------
void LocalAssembler::assemble_cell(Eigen::MatrixXd& A,
                                   UFC& ufc,
                                   const std::vector<double>& coordinate_dofs,
                                   const ufc::cell& ufc_cell,
                                   const Cell& cell,
                                   const MeshFunction<std::size_t>* domains)
{
  // Skip if there are no cell integrals
  if (!ufc.form.has_cell_integrals())
    return;

  // Extract default cell integral
  ufc::cell_integral* integral = ufc.default_cell_integral.get();

  // Get integral for sub domain (if any)
  if (domains && !domains->empty())
    integral = ufc.get_cell_integral((*domains)[cell]);

  // Skip integral if zero
  if (!integral)
    return;

  // Update to current cell
  ufc.update(cell, coordinate_dofs, ufc_cell,
             integral->enabled_coefficients());

  // Tabulate cell tensor
  integral->tabulate_tensor(ufc.A.data(),
                            ufc.w(),
                            coordinate_dofs.data(),
                            ufc_cell.orientation);

  // Stuff a_ufc.A into A
  const std::size_t M = A.rows();
  const std::size_t N = A.cols();
  for (std::size_t i = 0; i < M; i++)
    for (std::size_t j = 0; j < N; j++)
      A(i, j) += ufc.A[N*i + j];
}
//------------------------------------------------------------------------------
void LocalAssembler::assemble_exterior_facet(Eigen::MatrixXd& A,
                                  UFC& ufc,
                                  const std::vector<double>& coordinate_dofs,
                                  const ufc::cell& ufc_cell,
                                  const Cell& cell,
                                  const Facet& facet,
                                  const std::size_t local_facet,
                                  const MeshFunction<std::size_t>* domains)
{
  // Skip if there are no exterior facet integrals
  if (!ufc.form.has_exterior_facet_integrals())
    return;

  // Extract default exterior facet integral
  ufc::exterior_facet_integral* integral
    = ufc.default_exterior_facet_integral.get();

  // Get integral for sub domain (if any)
  if (domains && !domains->empty())
    integral = ufc.get_exterior_facet_integral((*domains)[facet]);

  // Skip integral if zero
  if (!integral)
    return;

  // Update to current cell
  ufc.update(cell, coordinate_dofs, ufc_cell,
             integral->enabled_coefficients());

  // Tabulate exterior facet tensor
  integral->tabulate_tensor(ufc.A.data(),
                            ufc.w(),
                            coordinate_dofs.data(),
                            local_facet,
                            ufc_cell.orientation);

  // Stuff a_ufc.A into A
  const std::size_t M = A.rows();
  const std::size_t N = A.cols();
  for (std::size_t i = 0; i < M; i++)
    for (std::size_t j = 0; j < N; j++)
      A(i, j) += ufc.A[N*i + j];
}
//------------------------------------------------------------------------------
void LocalAssembler::assemble_interior_facet(Eigen::MatrixXd& A,
                                  UFC& ufc,
                                  const std::vector<double>& coordinate_dofs,
                                  const ufc::cell& ufc_cell,
                                  const Cell& cell,
                                  const Facet& facet,
                                  const std::size_t local_facet,
								  const MeshFunction<std::size_t>* interior_facet_domains,
                                  const MeshFunction<std::size_t>* cell_domains)
{
  // Skip if there are no interior facet integrals
  if (!ufc.form.has_interior_facet_integrals())
	return;

  // Extract default interior facet integral
  ufc::interior_facet_integral* integral
	= ufc.default_interior_facet_integral.get();

  // Get integral for sub domain (if any)
  if (interior_facet_domains && !interior_facet_domains->empty())
	integral = ufc.get_interior_facet_integral((*interior_facet_domains)[facet]);

  // Skip integral if zero
  if (!integral)
	return;

  // Extract mesh
  const Mesh& mesh = cell.mesh();
  const std::size_t D = mesh.topology().dim();

  // Get cells incident with facet (which is 0 and 1 here is
  // arbitrary)
  dolfin_assert(facet.num_entities(D) == 2);
  std::size_t cell_index_plus = facet.entities(D)[0];
  std::size_t cell_index_minus = facet.entities(D)[1];

  //if the facet entity with cell_index_minus is current cell swap indices
  //This makes sure cell_index_plus represent the passed current cell
  if(cell_index_minus == cell.index())
  {
	 std::swap(cell_index_plus, cell_index_minus);
  }

  //cell_index_plus should now be the current cell which is already initialised.
  // Now we just need to initialise the neighbouring cell
  const Cell cell_adj(mesh, cell_index_minus);

  // Get local index of facet with respect to each cell
  std::size_t local_facet0 = local_facet;
  std::size_t local_facet1 = cell_adj.index(facet);

  std::vector<double> coordinate_dofs_adj;
  ufc::cell ufc_cell_adj;
  ufc::cell ufc_cell_0;

  // Update both cells, current cell has already passed the coordinate dofs
  cell.get_cell_data(ufc_cell_0, local_facet0);
  cell_adj.get_cell_data(ufc_cell_adj, local_facet1);
  cell_adj.get_coordinate_dofs(coordinate_dofs_adj);

  ufc.update(cell, coordinate_dofs, ufc_cell_0,
             cell_adj, coordinate_dofs_adj, ufc_cell_adj,
             integral->enabled_coefficients());


  // Update to current pair of cells and facets
//  ufc.update(cell0, *coordinate_dofs0, *ufc_cell0,
//			 cell1, *coordinate_dofs1, *ufc_cell1,
//			 integral->enabled_coefficients());

  //Normal is computed according to local_facet0
  integral->tabulate_tensor(ufc.macro_A.data(),
                            ufc.macro_w(),
                            coordinate_dofs.data(),
                            coordinate_dofs_adj.data(),
                            local_facet0,
                            local_facet1,
                            ufc_cell_0.orientation,
                            ufc_cell_adj.orientation);

  // Stuff upper left quadrant (corresponding to cell_plus) or lower
  // left quadrant (corresponding to cell_minus) into A depending on
  // which cell is the local cell
  const std::size_t M = A.rows();
  const std::size_t N = A.cols();

    if (N == 1)
    {
      for (std::size_t i = 0; i < M; i++)
        A(i, 0) += ufc.macro_A[i];
    }
    else
    {
      for (std::size_t i = 0; i < M; i++)
        for (std::size_t j = 0; j < N; j++)
          A(i, j) += ufc.macro_A[2*N*i + j];
    }

}
//------------------------------------------------------------------------------
