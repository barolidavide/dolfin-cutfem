/*
 * PatchMaker.cpp

 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/refinement/refine.h>
#include <dolfin/fem/fem_utils.h>
#include <dolfin/adaptivity/HatForm2D.h>
#include <dolfin/adaptivity/Patch.h>
#include <dolfin/parameter/Parameters.h>

#include "PatchMaker.h"

using namespace dolfin;

PatchMaker::PatchMaker(std::shared_ptr<dolfin::Mesh> mesh):_coarse_mesh(mesh)
{
	std::size_t gdim = _coarse_mesh->geometry().dim();

	_coarse_mesh->init(gdim,gdim-1);
	// parameters["refinement_algorithm"] = "plaza_with_parent_facets";
	 auto refined_mesh = dolfin::refine(*_coarse_mesh);

//	 _refined_mesh = std::make_shared<dolfin::Mesh>(refined_mesh);
//	 _parent_cell_map = refined_mesh.data().array("parent_cell", gdim);

	 //TODO: change this back
	 auto refined_mesh_2 = dolfin::refine(refined_mesh);

	 _refined_mesh = std::make_shared<dolfin::Mesh>(refined_mesh_2);

	  std::vector<std::size_t>& parent_cell_map
	    = refined_mesh.data().array("parent_cell", gdim);

	  std::vector<std::size_t>& parent_cell_map_2
	    = refined_mesh_2.data().array("parent_cell", gdim);

	  //Build map from grandchild to grandparent
	  _parent_cell_map = std::vector<std::size_t>(parent_cell_map_2.size());
	  for(std::size_t i=0;i<parent_cell_map_2.size();i++)
	  {
		  _parent_cell_map[i] = parent_cell_map[parent_cell_map_2[i]];
	  }
	 //TODO: end of change

	  // Make a map from parent->child cells
//	  std::vector<std::set<std::size_t>> reverse_cell_map(mesh->num_cells());
//	  for (dolfin::CellIterator cell(refined_mesh); !cell.end(); ++cell)
//	  {
//	    const std::size_t cell_index = cell->index();
//	    reverse_cell_map[parent_cell_map[cell_index]].insert(cell_index);
//	  }

	  //Make a reversed map from grandchild to grandparent
	  _reverse_cell_map = std::vector<std::set<std::size_t>>(mesh->num_cells());
	  for (dolfin::CellIterator cell(*_refined_mesh); !cell.end(); ++cell)
	  {
	    const std::size_t cell_index = cell->index();
	    _reverse_cell_map[_parent_cell_map[cell_index]].insert(cell_index);
	  }

	  //	  std::vector<std::size_t>& parent_facet_map
	  //	    = refined_mesh.data().array("parent_facet", gdim-1);
	  //
	  //	  std::cout << "parent_facet_map=[" ;
	  //	  for (dolfin::FacetIterator facet(*_refined_mesh); !facet.end(); ++facet)
	  //	  {
	  //	    const std::size_t facet_index = facet->index();
	  //	    std::cout << "facet: " << facet_index << ", parent facet: " << parent_facet_map[facet_index] << ",";
	  //
	  //	  }
	  //	  std::cout << "]" << std::endl;

	  //	  dolfin::File file_m0("./results/mesh0.pvd");
	  //	  dolfin::File file_m1("./results/mesh1.pvd");
	  //
	  //	  file_m0 << *_coarse_mesh;
	  //	  file_m1 << *_refined_mesh;

	  //	  std::cout << "parent_cell_map=[" ;
	  //	  for(std::size_t i=0;i<parent_cell_map.size();i++)
	  //	  {
	  //		  std::cout << parent_cell_map[i] << ",";
	  //	  }
	  //	  std::cout << "]" << std::endl;
	  //
	  //	  std::cout << "parent_cell_map_2=[" ;
	  //	  for(std::size_t i=0;i<parent_cell_map_2.size();i++)
	  //	  {
	  //		  std::cout << parent_cell_map_2[i] << ",";
	  //	  }
	  //	  std::cout << "]" << std::endl;
	  //
	  //	  std::cout << "grandparent_cell_map=[" ;
	  //	  for(std::size_t i=0;i<_parent_cell_map.size();i++)
	  //	  {
	  //		  std::cout << _parent_cell_map[i] << ",";
	  //	  }
	  //	  std::cout << "]" << std::endl;

//	  std::cout << "grand_reverse_cell_map=" << std::endl;
//	  for(std::size_t i=0;i<_reverse_cell_map.size();i++)
//	  {
//		  auto cell_map = _reverse_cell_map[i];
//		  std::cout << "grandparent cell " << i  << ": {";
//
//		   for ( auto it = cell_map.begin(); it != cell_map.end(); it++ )
//		   {
//		        std::cout << *it << ",";
//		   }
//
//		  std::cout << "}" << std::endl;
//	  }
}

void PatchMaker::create_vertex_patches()
{
	//Number of patches for fitted mesh
	_num_patches = _coarse_mesh->num_vertices();
	_patches = std::vector<dolfin::Patch>(_num_patches);

	  //Define linear continuous functionspace on coarse mesh to define hat function
	  auto V_hat = std::make_shared<HatForm2D::FunctionSpace>(_coarse_mesh);

	  // Get vertex to dof map
	  const std::vector<dolfin::la_index> vertex_to_dof_map = dolfin::vertex_to_dof_map(*V_hat);

	//Create patch around each vertex
	//  dolfin::tic();
	for(std::size_t i=0;i<_num_patches;i++)
	{
		std::size_t vertex_id = i;
		_patches[i].create_vertex_patch(_coarse_mesh,vertex_id);

	}
	//std::cout << "Time to create vertex patches:" << dolfin::toc() << std::endl;

	//dolfin::tic();
	for(std::size_t i=0;i<_num_patches;i++)
	{
		std::size_t vertex_id = i;
		_patches[i].build(_refined_mesh,_reverse_cell_map);
	}
	//std::cout << "Time to build markers for patches:" << dolfin::toc() << std::endl;

	//dolfin::tic();
	for(std::size_t i=0;i<_num_patches;i++)
	{
		std::size_t vertex_id = i;
		_patches[i].create_hat_function(V_hat, vertex_to_dof_map,vertex_id);
	}
	//std::cout << "Time to hat function:" << dolfin::toc() <<std::endl;

}




