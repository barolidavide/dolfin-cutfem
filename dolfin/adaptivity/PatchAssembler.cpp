/*
 * Patch.cpp
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */


#include "PatchAssembler.h"
#include "PatchErrorControl.h"
#include "Patch.h"
#include <dolfin/common/types.h>
#include <dolfin/common/Timer.h>
//#include <Eigen/Dense>

#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Facet.h>

#include <dolfin/fem/UFC.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/GenericDofMap.h>

#include <dolfin/adaptivity/LocalAssembler.h>

#include <dolfin/function/FunctionSpace.h>

#include <mpi.h>
#include <dolfin/log/log.h>

using namespace dolfin;

void PatchAssembler::assemble(Eigen::MatrixXd& A, const dolfin::Form& a
		, const Patch& patch
		, const std::unordered_map<std::size_t, std::size_t>& patch_dof_map)
{
	//Assign marker from patch to form
//	a.set_cell_domains(patch.cell_marker());
//	a.set_interior_facet_domains(patch.child_facet_marker());
//	a.set_exterior_facet_domains(patch.child_facet_marker());

	// Get cell domains
	std::shared_ptr<const dolfin::MeshFunction<std::size_t>>
	cell_domains = a.cell_domains();

	// Get exterior facet domains
	std::shared_ptr<const dolfin::MeshFunction<std::size_t>> exterior_facet_domains
	  = a.exterior_facet_domains();

	// Get interior facet domains
	std::shared_ptr<const dolfin::MeshFunction<std::size_t>> interior_facet_domains
	  = a.interior_facet_domains();

	// Get vertex domains
	std::shared_ptr<const dolfin::MeshFunction<std::size_t>> vertex_domains
	= a.vertex_domains();

	// Create data structure for local assembly data
	dolfin::UFC ufc(a);

	// Form rank
	const std::size_t form_rank = ufc.form.rank();

	// Update off-process coefficients
	const std::vector<std::shared_ptr<const dolfin::GenericFunction>>
		coefficients = a.coefficients();

	A.setZero();

	assemble_cells( A, a, ufc,  cell_domains, patch_dof_map, patch.child_cell_patch());

	assemble_exterior_facets(A,a, ufc, exterior_facet_domains, patch_dof_map, patch.exterior_facets_child_patch());

	assemble_interior_exterior_facets(A,a, ufc, exterior_facet_domains, cell_domains, patch_dof_map, patch.child_cell_patch(), patch.exterior_interior_facets_child_patch());

	assemble_interior_facets(A, a, ufc, interior_facet_domains, cell_domains, patch_dof_map, patch.exterior_interior_facets_child_patch());

//	std::cout << "Matrix A=" << A << std::endl;

}

//-----------------------------------------------------------------------------
void PatchAssembler::assemble_cells(
  Eigen::MatrixXd& A,
  const dolfin::Form& a,
  dolfin::UFC& ufc,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
  const std::set<size_t>& child_cell_patch)
{
  // Skip assembly if there are no cell integrals
  if (!ufc.form.has_cell_integrals())
    return;

  // Set timer
  dolfin::Timer timer("Assemble cells");

  // Extract mesh
  const dolfin::Mesh& mesh = a.mesh();

  // Form rank
  const std::size_t form_rank = ufc.form.rank();

  // Collect pointers to dof maps
  std::vector<const dolfin::GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dof map for a cell
  std::vector<dolfin::ArrayView<const dolfin::la_index>> dofs(form_rank);

  //Vector to hold mapped dofs of patch
  std::vector<std::vector<std::size_t>> dofs_p(form_rank);

  // Cell integral
  ufc::cell_integral* integral = ufc.default_cell_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();

  // Assemble over cells
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;

  for(auto it=child_cell_patch.begin(); it != child_cell_patch.end(); it++)
  {
	 std::size_t cell_index = *it;
	 auto cell = std::make_shared<dolfin::Cell>(mesh,cell_index);

    // Get integral for sub domain (if any)
    if (use_domains)
      integral = ufc.get_cell_integral((*domains)[*cell]);

    // Skip if no integral on current domain
    if (!integral)
      continue;

    // Check that cell is not a ghost
    dolfin_assert(!cell->is_ghost());

    // Update to current cell
    cell->get_cell_data(ufc_cell);
    cell->get_coordinate_dofs(coordinate_dofs);
    ufc.update(*cell, coordinate_dofs, ufc_cell,
               integral->enabled_coefficients());

    // Get local-to-global dof maps for cell
    bool empty_dofmap = false;
    for (std::size_t i = 0; i < form_rank; ++i)
    {
      dofs[i] = dofmaps[i]->cell_dofs(cell->index());
      empty_dofmap = empty_dofmap || dofs[i].size() == 0;
    }

    //Get patch dofs through patch dof map
    for (std::size_t i = 0; i < form_rank; ++i)
	{
    	std::size_t dof_size = dofs[i].size();
    	dofs_p[i] = std::vector<std::size_t>(dof_size);
    	for (std::size_t j = 0; j < dof_size; ++j)
    	{
    		std::size_t dof_n = dofs[i][j];
    		auto it = patch_dof_map.find(dof_n);
    		if (it != patch_dof_map.end())
    			dofs_p[i][j]=it->second;
    		else
    		{
    		    dolfin::dolfin_error("dof is not found in dofmap",
    		                         "for",
    		                         "patch");
    		}
    	}
	}

    // Skip if at least one dofmap is empty
    if (empty_dofmap)
      continue;

    // Tabulate cell tensor
    integral->tabulate_tensor(ufc.A.data(), ufc.w(),
                              coordinate_dofs.data(),
                              ufc_cell.orientation);

    //Copy result into matrix A
    // Stuff a_ufc.A into A
    if(form_rank == 2)
    {
		const std::size_t M = dofs[0].size();
		const std::size_t N = dofs[1].size();
		for (std::size_t i = 0; i < M; i++)
		{
		  for (std::size_t j = 0; j < N; j++)
		  {
			  std::size_t i_index = dofs_p[0][i];
			  std::size_t j_index = dofs_p[1][j];
			  A(i_index, j_index) += ufc.A[N*i + j];
		  }
		}
    }
    //Form rank = 1
    else
    {
    	const std::size_t M = dofs[0].size();
		const std::size_t N = 1;
		std::size_t j_index = 0;
		for (std::size_t i = 0; i < M; i++)
		{
		  for (std::size_t j = 0; j < N; j++)
		  {
			  std::size_t i_index = dofs_p[0][i];
			  A(i_index, j_index) += ufc.A[N*i + j];
		  }
		}
    }
  }
}

void PatchAssembler::assemble_exterior_facets(
  Eigen::MatrixXd& A,
  const dolfin::Form& a,
  dolfin::UFC& ufc,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
  const std::set<std::size_t>& exterior_facets_child_patch)
{
  // Skip assembly if there are no exterior facet integrals
  if (!ufc.form.has_exterior_facet_integrals())
    return;

  // Set timer
  dolfin::Timer timer("Assemble exterior facets");

  // Extract mesh
  const dolfin::Mesh& mesh = a.mesh();

  // Form rank
  const std::size_t form_rank = ufc.form.rank();

  // Collect pointers to dof maps
  std::vector<const dolfin::GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dof map for a cell
  std::vector<dolfin::ArrayView<const dolfin::la_index>> dofs(form_rank);

  //Vector to hold mapped dofs of patch
  std::vector<std::vector<std::size_t>> dofs_p(form_rank);

  // Exterior facet integral
  const ufc::exterior_facet_integral* integral
    = ufc.default_exterior_facet_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();

  // Compute facets and facet - cell connectivity if not already computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(D - 1);
  mesh.init(D - 1, D);
  dolfin_assert(mesh.ordered());

  // Assemble over exterior facets (the cells of the boundary)
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;

  //for (FacetIterator facet(mesh); !facet.end(); ++facet)
  for(auto it=exterior_facets_child_patch.begin(); it != exterior_facets_child_patch.end(); it++)
  {
	  std::size_t facet_index = *it;
	  auto facet = std::make_shared<dolfin::Facet>(mesh,facet_index);

    // Only consider exterior facets
    if (!facet->exterior())
    {
      continue;
    }

    // Get integral for sub domain (if any)
    if (use_domains)
      integral = ufc.get_exterior_facet_integral((*domains)[*facet]);

    // Skip integral if zero
    if (!integral)
      continue;

    // Get mesh cell to which mesh facet belongs (pick first, there is
    // only one)
    dolfin_assert(facet->num_entities(D) == 1);
    dolfin::Cell mesh_cell(mesh, facet->entities(D)[0]);

    // Check that cell is not a ghost
    dolfin_assert(!mesh_cell.is_ghost());

    // Get local index of facet with respect to the cell
    const std::size_t local_facet = mesh_cell.index(*facet);

    // Update UFC cell
    mesh_cell.get_cell_data(ufc_cell, local_facet);
    mesh_cell.get_coordinate_dofs(coordinate_dofs);

    // Update UFC object
    ufc.update(mesh_cell, coordinate_dofs, ufc_cell,
               integral->enabled_coefficients());

    // Get local-to-global dof maps for cell
    for (std::size_t i = 0; i < form_rank; ++i)
      dofs[i] = dofmaps[i]->cell_dofs(mesh_cell.index());

    //Get patch dofs through patch dof map
    for (std::size_t i = 0; i < form_rank; ++i)
	{
    	std::size_t dof_size = dofs[i].size();
    	dofs_p[i] = std::vector<std::size_t>(dof_size);
    	for (std::size_t j = 0; j < dof_size; ++j)
    	{
    		std::size_t dof_n = dofs[i][j];
    		auto it = patch_dof_map.find(dof_n);
			if (it != patch_dof_map.end())
				dofs_p[i][j]=it->second;
			else
			{
				//TODO: this is dangerous, this is a dummy dof as cell will have 3 dofs but only 2 along facet
				// contribution to this dof in integral should be zero
				dolfin::dolfin_error("PatchAssembler.cpp, assemble_exterior_facets",
									 "dof of cell is accessed that is not in patch",
									 "exterior facets still preliminary");

//				dofs_p[i][j]= 0;
//				std::cout << "Warning dummy dof is used here, values might be buggy!!!" << std::endl;
//				std::cout << "dof_n=" << dof_n << std::endl;
//				dolfin::dolfin_error("dof is not found in dofmap",
//									 "for",
//									 "patch");
			}
    	}
	}

    // Tabulate exterior facet tensor
    integral->tabulate_tensor(ufc.A.data(),
                              ufc.w(),
                              coordinate_dofs.data(),
                              local_facet,
                              ufc_cell.orientation);

    // Add entries to global tensor
    //Copy result into matrix A
       // Stuff a_ufc.A into A
       if(form_rank == 2)
       {
   		const std::size_t M = dofs[0].size();
   		const std::size_t N = dofs[1].size();
   		for (std::size_t i = 0; i < M; i++)
   		{
   		  for (std::size_t j = 0; j < N; j++)
   		  {
   			  std::size_t i_index = dofs_p[0][i];
   			  std::size_t j_index = dofs_p[1][j];
   			  A(i_index, j_index) += ufc.A[N*i + j];
   		  }
   		}
       }
       //Form rank = 1
       else
       {
       	const std::size_t M = dofs[0].size();
   		const std::size_t N = 1;
   		std::size_t j_index = 0;
   		for (std::size_t i = 0; i < M; i++)
   		{
   		  for (std::size_t j = 0; j < N; j++)
   		  {
   			  std::size_t i_index = dofs_p[0][i];
   			  A(i_index, j_index) += ufc.A[N*i + j];
   		  }
   		}
       }
  }
}

void PatchAssembler::assemble_interior_exterior_facets(
  Eigen::MatrixXd& A,
  const dolfin::Form& a,
  dolfin::UFC& ufc,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> cell_domains,
  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
  const std::set<size_t>& child_cell_patch,
  const std::set<std::size_t>& exterior_facets_child_patch)
{
  // Skip assembly if there are no exterior facet integrals
  if (!ufc.form.has_exterior_facet_integrals())
    return;

  // Set timer
  dolfin::Timer timer("Assemble exterior facets");

  // Extract mesh
  const dolfin::Mesh& mesh = a.mesh();

  // Form rank
  const std::size_t form_rank = ufc.form.rank();

  // Collect pointers to dof maps
  std::vector<const dolfin::GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dof map for a cell
  std::vector<dolfin::ArrayView<const dolfin::la_index>> dofs(form_rank);

  //Vector to hold mapped dofs of patch
  std::vector<std::vector<std::size_t>> dofs_p(form_rank);

  // Exterior facet integral
  const ufc::exterior_facet_integral* integral
    = ufc.default_exterior_facet_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();
  bool use_cell_domains = cell_domains && !cell_domains->empty();

  // Compute facets and facet - cell connectivity if not already computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(D - 1);
  mesh.init(D - 1, D);
  dolfin_assert(mesh.ordered());

  // Assemble over exterior facets (the cells of the boundary)
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;

  //for (FacetIterator facet(mesh); !facet.end(); ++facet)
  for(auto it=exterior_facets_child_patch.begin(); it != exterior_facets_child_patch.end(); it++)
  {
	  std::size_t facet_index = *it;
	  auto facet = std::make_shared<dolfin::Facet>(mesh,facet_index);

    // Only consider exterior facets
//    if (!facet->exterior())
//    {
//      continue;
//    }

	// Get cells incident with facet (which is 0 and 1 here is arbitrary)
	dolfin_assert(facet->num_entities(D) == 2);
	std::size_t cell_index_plus = facet->entities(D)[0];
	std::size_t cell_index_minus = facet->entities(D)[1];

	//Check which of the cells adjacent to the facet is in the patch
	// make sure cell_index_plus indicates interior to the element in the patch
	// if cell_index_minus is in the patch swap + and - as + indicates interior to patch
	if(child_cell_patch.find(cell_index_minus)!= child_cell_patch.end())
	{
	  std::swap(cell_index_plus, cell_index_minus);
	}

    // Get integral for sub domain (if any)
    if (use_domains)
      integral = ufc.get_exterior_facet_integral((*domains)[*facet]);

    // Skip integral if zero
    if (!integral)
      continue;

    // Get mesh cell to which mesh facet belongs (pick first, there is
    // only one)
	//Continue working with the cell interior to the patch
    dolfin::Cell mesh_cell(mesh,  cell_index_plus);

    // Check that cell is not a ghost
    dolfin_assert(!mesh_cell.is_ghost());

    // Get local index of facet with respect to the cell
    const std::size_t local_facet = mesh_cell.index(*facet);

    // Update UFC cell
    mesh_cell.get_cell_data(ufc_cell, local_facet);
    mesh_cell.get_coordinate_dofs(coordinate_dofs);

    // Update UFC object
    ufc.update(mesh_cell, coordinate_dofs, ufc_cell,
               integral->enabled_coefficients());

    // Get local-to-global dof maps for cell
    for (std::size_t i = 0; i < form_rank; ++i)
      dofs[i] = dofmaps[i]->cell_dofs(mesh_cell.index());

    //Get patch dofs through patch dof map
    for (std::size_t i = 0; i < form_rank; ++i)
	{
    	std::size_t dof_size = dofs[i].size();
    	dofs_p[i] = std::vector<std::size_t>(dof_size);
    	for (std::size_t j = 0; j < dof_size; ++j)
    	{
    		std::size_t dof_n = dofs[i][j];
    		auto it = patch_dof_map.find(dof_n);
			if (it != patch_dof_map.end())
				dofs_p[i][j]=it->second;
			else
			{
				//TODO: this is dangerous, this is a dummy dof as cell will have 3 dofs but only 2 along facet
				// contribution to this dof in integral should be zero
				dolfin::dolfin_error("PatchAssembler.cpp, assemble_interior_exterior_facets",
													 "find the macro dof in the patch dofmap",
													 "interior facets still preliminary");
			}
    	}
	}

    // Tabulate exterior facet tensor
    integral->tabulate_tensor(ufc.A.data(),
                              ufc.w(),
                              coordinate_dofs.data(),
                              local_facet,
                              ufc_cell.orientation);

    // Add entries to global tensor
    //Copy result into matrix A
       // Stuff a_ufc.A into A
       if(form_rank == 2)
       {
   		const std::size_t M = dofs[0].size();
   		const std::size_t N = dofs[1].size();
   		for (std::size_t i = 0; i < M; i++)
   		{
   		  for (std::size_t j = 0; j < N; j++)
   		  {
   			  std::size_t i_index = dofs_p[0][i];
   			  std::size_t j_index = dofs_p[1][j];
   			  A(i_index, j_index) += ufc.A[N*i + j];
   		  }
   		}
       }
       //Form rank = 1
       else
       {
       	const std::size_t M = dofs[0].size();
   		const std::size_t N = 1;
   		std::size_t j_index = 0;
   		for (std::size_t i = 0; i < M; i++)
   		{
   		  for (std::size_t j = 0; j < N; j++)
   		  {
   			  std::size_t i_index = dofs_p[0][i];
   			  A(i_index, j_index) += ufc.A[N*i + j];
   		  }
   		}
       }
  }
}

//-----------------------------------------------------------------------------
void PatchAssembler::assemble_interior_facets(
  Eigen::MatrixXd& A,
  const dolfin::Form& a,
  dolfin::UFC& ufc,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> cell_domains,
  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
  const std::set<std::size_t>& interior_facets_child_patch)
{
  // Skip assembly if there are no interior facet integrals
  if (!ufc.form.has_interior_facet_integrals())
    return;

  // Set timer
  dolfin::Timer timer("Assemble interior facets");

  // Extract mesh and coefficients
  const dolfin::Mesh& mesh = a.mesh();

  // MPI rank
  const int my_mpi_rank = dolfin::MPI::rank(mesh.mpi_comm());

  // Form rank
  const std::size_t form_rank = ufc.form.rank();

  // Collect pointers to dof maps
  std::vector<const dolfin::GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dofs for cells, and a vector holding pointers to same
  std::vector<std::vector<dolfin::la_index>> macro_dofs(form_rank);
  std::vector<dolfin::ArrayView<const dolfin::la_index>> macro_dof_ptrs(form_rank);

  //Vector to hold mapped dofs of patch
  std::vector<std::vector<std::size_t>> dofs_p(form_rank);

  // Interior facet integral
  const ufc::interior_facet_integral* integral
    = ufc.default_interior_facet_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();
  bool use_cell_domains = cell_domains && !cell_domains->empty();

  // Compute facets and facet - cell connectivity if not already computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(D - 1);
  mesh.init(D - 1, D);
  dolfin_assert(mesh.ordered());

  // Assemble over interior facets (the facets of the mesh)
  ufc::cell ufc_cell[2];
  std::vector<double> coordinate_dofs[2];

  //for (FacetIterator facet(mesh); !facet.end(); ++facet)
  for(auto it=interior_facets_child_patch.begin(); it != interior_facets_child_patch.end(); it++)
  {
	std::size_t facet_index = *it;
	auto facet = std::make_shared<dolfin::Facet>(mesh,facet_index);

    if (facet->num_entities(D) == 1)
      continue;

    // Check that facet is not a ghost
    dolfin_assert(!facet->is_ghost());

    // Get integral for sub domain (if any)
    if (use_domains)
      integral = ufc.get_interior_facet_integral((*domains)[*facet]);

    // Skip integral if zero
    if (!integral)
      continue;

    // Get cells incident with facet (which is 0 and 1 here is arbitrary)
    dolfin_assert(facet->num_entities(D) == 2);
    std::size_t cell_index_plus = facet->entities(D)[0];
    std::size_t cell_index_minus = facet->entities(D)[1];

    if (use_cell_domains && (*cell_domains)[cell_index_plus]
        < (*cell_domains)[cell_index_minus])
    {
      std::swap(cell_index_plus, cell_index_minus);
    }

    // The convention '+' = 0, '-' = 1 is from ffc
    const dolfin::Cell cell0(mesh, cell_index_plus);
    const dolfin::Cell cell1(mesh, cell_index_minus);

    // Get local index of facet with respect to each cell
    std::size_t local_facet0 = cell0.index(*facet);
    std::size_t local_facet1 = cell1.index(*facet);

    // Update to current pair of cells
    cell0.get_cell_data(ufc_cell[0], local_facet0);
    cell0.get_coordinate_dofs(coordinate_dofs[0]);
    cell1.get_cell_data(ufc_cell[1], local_facet1);
    cell1.get_coordinate_dofs(coordinate_dofs[1]);

    ufc.update(cell0, coordinate_dofs[0], ufc_cell[0],
               cell1, coordinate_dofs[1], ufc_cell[1],
               integral->enabled_coefficients());

    // Tabulate dofs for each dimension on macro element
    for (std::size_t i = 0; i < form_rank; i++)
    {
      // Get dofs for each cell
      const dolfin::ArrayView<const dolfin::la_index> cell_dofs0
        = dofmaps[i]->cell_dofs(cell0.index());
      const dolfin::ArrayView<const dolfin::la_index> cell_dofs1
        = dofmaps[i]->cell_dofs(cell1.index());

      // Create space in macro dof vector
      macro_dofs[i].resize(cell_dofs0.size() + cell_dofs1.size());

      // Copy cell dofs into macro dof vector
      std::copy(cell_dofs0.data(), cell_dofs0.data() + cell_dofs0.size(),
                macro_dofs[i].begin());
      std::copy(cell_dofs1.data(), cell_dofs1.data() + cell_dofs1.size(),
                macro_dofs[i].begin() + cell_dofs0.size());
      macro_dof_ptrs[i].set(macro_dofs[i]);
    }

    // Tabulate interior facet tensor on macro element
    integral->tabulate_tensor(ufc.macro_A.data(),
                              ufc.macro_w(),
                              coordinate_dofs[0].data(),
                              coordinate_dofs[1].data(),
                              local_facet0,
                              local_facet1,
                              ufc_cell[0].orientation,
                              ufc_cell[1].orientation);

    if (cell0.is_ghost() != cell1.is_ghost())
    {
      int ghost_rank = -1;
      if (cell0.is_ghost())
        ghost_rank = cell0.owner();
      else
        ghost_rank = cell1.owner();

      dolfin_assert(my_mpi_rank != ghost_rank);
      dolfin_assert(ghost_rank != -1);
      if (ghost_rank < my_mpi_rank)
        continue;
    }

    //Map macro dofs to patch dofs
    //Get patch dofs through patch dof map
    for (std::size_t i = 0; i < form_rank; ++i)
	{
    	std::size_t dof_size = macro_dof_ptrs[i].size();
    	dofs_p[i] = std::vector<std::size_t>(dof_size);
    	for (std::size_t j = 0; j < dof_size; ++j)
    	{
    		std::size_t dof_n = macro_dof_ptrs[i][j];
    		auto it = patch_dof_map.find(dof_n);
			if (it != patch_dof_map.end())
				dofs_p[i][j]=it->second;
			else
			{
				dolfin::dolfin_error("PatchAssembler.cpp, assemble_interior_facets",
									 "find the macro dof in the patch dofmap",
									 "interior facets still preliminary");
			}
    	}
	}
    // Add entries to global tensor
   // A.add_local(ufc.macro_A.data(), macro_dof_ptrs);
    // Stuff a_ufc.A into A
    if(form_rank == 2)
    {
		const std::size_t M = macro_dof_ptrs[0].size();
		const std::size_t N = macro_dof_ptrs[1].size();
		for (std::size_t i = 0; i < M; i++)
		{
		  for (std::size_t j = 0; j < N; j++)
		  {
			  std::size_t i_index = dofs_p[0][i];
			  std::size_t j_index = dofs_p[1][j];
			  A(i_index, j_index) += ufc.macro_A[2*N*i + j]; //Does this have to be N*i + j or 2*N*i + j
		  }
		}
    }
    //Form rank = 1
    else
    {
    	const std::size_t M = macro_dof_ptrs[0].size();
		const std::size_t N = 1;
		std::size_t j_index = 0;
		for (std::size_t i = 0; i < M; i++)
		{
		  for (std::size_t j = 0; j < N; j++)
		  {
			  std::size_t i_index = dofs_p[0][i];
			  A(i_index, j_index) += ufc.macro_A[N*i + j];
		  }
		}
    }

  }
}

//-----------------------------------------------------------------------------
void PatchAssembler::assemble_local(const dolfin::Form& a,
                            const dolfin::Cell& cell,
                            std::vector<double>& tensor)
{
  //Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> A_e;
  Eigen::MatrixXd A_e;
  dolfin::UFC ufc(a);
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;

  // Get size of local tensor
  std::size_t N, M;
  if (a.rank() == 0)
  {
    N = 1;
    M = 1;
  }
  else if (a.rank() == 1)
  {
    N = a.function_space(0)->dofmap()->cell_dofs(cell.index()).size();
    M = 1;
  }
  else
  {
    N = a.function_space(0)->dofmap()->cell_dofs(cell.index()).size();
    M = a.function_space(1)->dofmap()->cell_dofs(cell.index()).size();
  }

  // Extract cell_domains etc from the form
  const dolfin::MeshFunction<std::size_t>* cell_domains =
    a.cell_domains().get();
  const dolfin::MeshFunction<std::size_t>* exterior_facet_domains =
    a.exterior_facet_domains().get();
  const dolfin::MeshFunction<std::size_t>* interior_facet_domains =
    a.interior_facet_domains().get();

  // Update to the local cell and assemble
  A_e.resize(N, M);
  cell.get_coordinate_dofs(coordinate_dofs);
  dolfin::LocalAssembler::assemble(A_e, ufc, coordinate_dofs,
                           ufc_cell, cell, cell_domains,
                           exterior_facet_domains, interior_facet_domains);

  // Copy values into the return tensor
  tensor.resize(N*M);
  for (std::size_t i = 0; i < N; i++)
    for (std::size_t j = 0; j < M; j++)
      tensor[i*M+j] = A_e(i,j);
}
