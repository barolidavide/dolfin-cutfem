/*
 * Patch.h
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */

#ifndef __PATCHERRORCONTROL_H_
#define __PATCHERRORCONTROL_H_

#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/common/types.h>
#include <dolfin/adaptivity/Patch.h>
#include <unordered_map>

namespace dolfin
{
  class Mesh;
  template<typename T> class MeshFunction;
  class GenericFunction;
  class FunctionSpace;
  class Point;
  class FiniteElement;
  class Form;
	class PatchMaker;

	class PatchErrorControl
	{
		public:
			//static void build_global_to_patch_dofmap(const std::shared_ptr<const dolfin::FunctionSpace> V, const Patch &patch, std::unordered_map<std::size_t, std::size_t>& patch_dof_map);
			static void build_global_to_patch_dofmap(const std::shared_ptr<const dolfin::FunctionSpace> V, const Patch &patch,std::set<std::size_t>& patch_dofs, std::unordered_map<std::size_t, std::size_t>& patch_dof_map);
			static void solve(const dolfin::Form& a_r,dolfin::Function& u, dolfin::Form& L_r, const PatchMaker& patch_maker);
			static void solve_density_per_patch(const dolfin::Form& a_dual_r, dolfin::Function & u, dolfin::Form& L_dual_r, dolfin::Form& L_r, const PatchMaker& patch_maker);
			static void solve_local(const dolfin::Form& a_R_T, const dolfin::Form& L_R_T, dolfin::Function& R_T);
			static void solve_per_patch(const dolfin::Form& a_dual_r, dolfin::Function & R_ez, dolfin::Form& L_dual_r, dolfin::Form& L_r, const PatchMaker& patch_maker);

			static void compute_indicators(dolfin::MeshFunction<double>& indicators,
                    dolfin::Function& error_density);
			static void compute_indicators_DG0(dolfin::MeshFunction<double>& indicators,
			                                       dolfin::Function& eta);

			static void compute_R_ez_T(const dolfin::Form& a, dolfin::Function& R_T);
			static void get_restricted_vertex_dofs_DG_space(const std::shared_ptr<dolfin::FunctionSpace> DG2, std::vector<dolfin::la_index> &vertex_dofs);
			static void apply_restriction(dolfin::GenericMatrix* A,
			        dolfin::GenericVector* b, const std::vector<dolfin::la_index> &vertex_dofs, const std::shared_ptr<dolfin::FunctionSpace> DG2);
			static void restrict_vertices_DG2(dolfin::GenericMatrix& A,
					dolfin::GenericVector& b, const std::shared_ptr<dolfin::FunctionSpace> DG2);



		private:

	};
}




#endif /* CUTFEM_ADAPTIVITY_PATCH_H_ */
