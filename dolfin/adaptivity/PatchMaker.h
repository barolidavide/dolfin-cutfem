/*
 * PatchMaker.h
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */

#ifndef __PATCHMAKER_H_
#define __PATCHMAKER_H_

#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/adaptivity/Patch.h>

namespace dolfin
{
  class Mesh;
  template<typename T> class MeshFunction;
  class GenericFunction;
  class Point;
  class FiniteElement;

	class PatchMaker
	{
		public:
			PatchMaker(std::shared_ptr<dolfin::Mesh> mesh);
			void create_vertex_patches();

			const std::shared_ptr<dolfin::Mesh> coarse_mesh() const
			{
				return _coarse_mesh;
			}

			const std::shared_ptr<dolfin::Mesh> refined_mesh() const
			{
				return _refined_mesh;
			}

			const std::vector<std::set<std::size_t>> reverse_cell_map() const
			{
				return _reverse_cell_map;
			}

			const std::size_t num_patches() const
			{
				return _num_patches;
			}

			const std::vector<dolfin::Patch>& patches() const
			{
				return _patches;
			}


		private:
			//Coarsest mesh used to solve original problem
			std::shared_ptr<dolfin::Mesh> _coarse_mesh;
			//Finest mesh used to solve error estimate
			std::shared_ptr<dolfin::Mesh> _refined_mesh;

			//Maps from child in refined_mesh to parent in coarse mesh
			std::vector<std::size_t> _parent_cell_map;
			//Maps from parent in coarse mesh to children in child mesh
			std::vector<std::set<std::size_t>> _reverse_cell_map;

			//Number of patches
			std::size_t _num_patches;
			//Vector of patches
			std::vector<dolfin::Patch> _patches;
	};
}



#endif /* CUTFEM_ADAPTIVITY_PATCHMAKER_H_ */
