from dolfin import *
from mshr import *
import numpy as np

parameters["refinement_algorithm"] = "plaza_with_parent_facets"

N = 10

#Output files
file_ez = File("./results/ez_dg.pvd")
file_r_K = File("./results/R_ez_K.pvd")
mesh_file = File("./results/mesh.pvd")
file_uh = File("./results/u_h.pvd")
file_uexact = File("./results/u_exact.pvd")
file_zh = File("./results/z_h.pvd")
file_boundaries = File("./results/boundaries.pvd")
refinement_marker_file = File("./results/refinement_marker.pvd");
indicator_file = File("./results/indicators.pvd");

rectangle = Rectangle(Point(-1.0,-1.0),Point(1.0,1.0))
small_rectangle = Rectangle(Point(-1.0,-1.0),Point(0.0,0.0))

geometry = rectangle - small_rectangle

# Create mesh
mesh = generate_mesh(geometry, N)

class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0) or near(x[1], 1.0) and on_boundary

class GoalBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], -1.0) and on_boundary

class VerticalNeumannBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0) and x[1] < 0+DOLFIN_EPS and on_boundary

class EntireBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

dirchlet_bc = DirichletBoundary()
goal_boundary = GoalBoundary()
entire_boundary = EntireBoundary()
vertical_neumann_boundary = VerticalNeumannBoundary()

n = FacetNormal(mesh)
    
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
boundaries.set_all(0)
entire_boundary.mark(boundaries, 1)
dirchlet_bc.mark(boundaries, 0)
goal_boundary.mark(boundaries, 2)

#Define measures with marked boundary
ds = Measure('ds', domain = mesh, subdomain_data = boundaries)
#ds = Measure("ds")[boundaries]

f = Expression("-2.0*(x[0]-1)")
g = Expression(("pow((x[1]-1),2.0)","2.0*(x[0]-1.0)*(x[1]-1.0)"),degree=2)
u_exact =  Expression("(x[0]-1.0)*pow((x[1]-1),2.0)",degree=2)

V = FunctionSpace(mesh, "Lagrange", 1)

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, dirchlet_bc)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)

a = inner(grad(u), grad(v))*dx()
L = f*v*dx() + dot(g,n)*v*(ds(1)+ds(2))

# Define function for the solution
u = Function(V)

# Define goal functional (quantity of interest)
M = u*ds(2)

# Define error tolerance
tol = 1.e-5

# Solve equation a = L with respect to u and the given boundary
# conditions, such that the estimated error (measured in M) is less
# than tol
problem = LinearVariationalProblem(a, L, u, bc)
solver = AdaptiveLinearVariationalSolver(problem, M)
#solver.parameters["error_control"]["dual_variational_solver"]["linear_solver"] = "cg"
#solver.parameters["error_control"]["dual_variational_solver"]["symmetric"] = True
solver.solve(tol)

data = solver.adaptive_data()

#print "M_u_h=", datum["functional_value"]

solver.summary()

# Plot solution(s)
File("u_coarse.pvd") << u.root_node()
File("u_fine.pvd") << u.leaf_node()
plot(u.root_node(), title="Solution on initial mesh")
plot(u.leaf_node(), title="Solution on final mesh")
interactive()
