from dolfin import *
from mshr import *
import numpy as np

N = 10

#Output files
file_ez = File("./results/ez_dg.pvd")
file_r_K = File("./results/R_ez_K.pvd")
mesh_file = File("./results/mesh.pvd")
file_uh = File("./results/u_h.pvd")
file_uexact = File("./results/u_exact.pvd")
file_zh = File("./results/z_h.pvd")
file_boundaries = File("./results/boundaries.pvd")
refinement_marker_file = File("./results/refinement_marker.pvd");
indicator_file = File("./results/indicators.pvd");

rectangle = Rectangle(Point(-1.0,-1.0),Point(1.0,1.0))
small_rectangle = Rectangle(Point(-1.0,-1.0),Point(0.0,0.0))

geometry = rectangle - small_rectangle

# Create mesh
mesh = generate_mesh(geometry, N)

class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0) or near(x[1], 1.0) and on_boundary

class GoalBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], -1.0) and on_boundary

class VerticalNeumannBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0) and x[1] < 0+DOLFIN_EPS and on_boundary

class EntireBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

dirchlet_bc = DirichletBoundary()
goal_boundary = GoalBoundary()
entire_boundary = EntireBoundary()
vertical_neumann_boundary = VerticalNeumannBoundary()

f = Expression("-2.0*(x[0]-1)")
g = Expression(("pow((x[1]-1),2.0)","2.0*(x[0]-1.0)*(x[1]-1.0)"),degree=2)
u_exact =  Expression("(x[0]-1.0)*pow((x[1]-1),2.0)",degree=2)

N_refinements = 13
N_array = np.zeros(N_refinements)
efficiency_array = np.zeros(N_refinements)
eta_ex = np.zeros(N_refinements)
eta_h = np.zeros(N_refinements)

for i in range(0, N_refinements):
    
    mesh_file << mesh

    n = FacetNormal(mesh)

    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    entire_boundary.mark(boundaries, 1)
    dirchlet_bc.mark(boundaries, 0)
    goal_boundary.mark(boundaries, 2)
    #vertical_neumann_boundary.mark(boundaries,3)
    #patch_maker = PatchMaker(mesh)

    #Define measures with marked boundary
    ds = Measure('ds', domain = mesh, subdomain_data = boundaries)

    #################################################
    ## Step 0: Solve primal problem on coarse mesh ##
    #################################################
    V = FunctionSpace(mesh,"CG",1)

    u_exact_func = Function(V)
    u_exact_func.interpolate(u_exact)

    u = TrialFunction(V)
    v = TestFunction(V)

    def a(u,v):
        a = inner(grad(u),grad(v))*dx
        return a

    def l(v):
        L = f*v*dx + dot(g,n)*v*(ds(1)+ds(2))
        return L

    def a_d(v,u):
        a = inner(grad(v),grad(u))*dx
        return a

    def Q(u):
        Q = u*ds(2)
        return Q

    a_primal = a(u,v)
    L_primal = l(v)

    u0 = Constant(0.0)
    bc = DirichletBC(V, u0,dirchlet_bc)

    u_h = Function(V)

    solve(a_primal == L_primal, u_h, bcs=bc)


    #################################################
    ## Step 1: Solve dual problem on coarse mesh   ##
    #################################################
    a_dual = a_d(v,u) #adjoint(a_primal)
    L_dual = Q(v)

    z_h = Function(V)

    solve(a_dual == L_dual, z_h, bcs=bc)

    #############################################################################################
    ## Step 2: Solve error equation approximately using a restricted DG2 space                 ##
    #############################################################################################
    V_dg2 = FunctionSpace(mesh,"DG",2)
    ez = TrialFunction(V_dg2)
    v_dg = TestFunction(V_dg2)

    gamma = 1000
    h = 2*Circumradius(mesh)

    a_e = a_d(v_dg,ez)
    a_e -= inner(dot(grad(ez),n), v_dg)*ds(0)
    a_e -= inner(dot(grad(v_dg),n), ez)*ds(0)
    a_e += gamma/h*inner(ez, v_dg)*ds(0)

    L_e = Q(v_dg)-a_d(v_dg,z_h)
    L_e += inner(0.5*(dot(grad(z_h('-')),n('+'))+dot(grad(z_h('+')),n('+'))),jump(v_dg))*dS
    #L_e += inner(dot(grad(z_h),n),v_dg)*ds

    A_e = assemble(a_e)
    b_e = assemble(L_e)

    patch_ec = PatchErrorControl()
    patch_ec.restrict_vertices_DG2(A_e,b_e,V_dg2)

    ez = Function(V_dg2)
    solve(A_e,ez.vector(),b_e);

    #############################################################################################
    ## Step 3: Compute the dual weighted Residual                                              ##
    #############################################################################################
    V_dg0 = FunctionSpace(mesh,"DG",0)
    R_ez_K = Function(V_dg0)

    L_r  = l(ez) - a(u_h,ez)
    L_r += inner(0.5*(dot(grad(u_h('-')),n('+'))+dot(grad(u_h('+')),n('+'))),ez('+'))*dS
    #L_r += inner(dot(grad(u_h),n),ez)*ds

    L_r = Form(L_r)
    patch_ec.compute_R_ez_T(L_r, R_ez_K)

    eta_exact = assemble(Q(u_h)) #assemble(Q(u_exact)-Q(u_h))
    eta_exact = abs(-2.0/3.0-eta_exact)
    sum_eta_T = R_ez_K.vector().sum()

    #############################################################################################
    ## Step 3: Compute indicators and markers and refine mesh                                  ##
    #############################################################################################
    indicators = CellFunction("double",mesh);
    patch_ec.compute_indicators_DG0(indicators,R_ez_K);
        
    markers = CellFunction("bool",mesh)
    strategy = "dorfler";
    fraction = 0.5;
    mark(markers, indicators, strategy, fraction)

    mesh = refine(mesh,markers)

    #####################################################################
    ##  Output results                                                 ##
    #####################################################################
    
    N_array[i] = V.dofmap().global_dimension()
    eta_ex[i] = eta_exact
    eta_h[i] = sum_eta_T
    efficiency_array[i] = sum_eta_T/eta_exact

    print "N=", N_array[i]
    print "eta_exact=", eta_exact
    print "sum_eta_T=", sum_eta_T
    print "efficiency=", sum_eta_T/eta_exact

#    N_ref = 1
#    refined_mesh = refine(mesh)
#    for j in range(0, N_ref):
#        refined_mesh = refine(refined_mesh)
#
#    V_f_proj = FunctionSpace(refined_mesh,"CG",1)
#    ez_dg_proj = Function(V_f_proj)
#    ez_dg_proj.interpolate(ez)


    # Save to file and plot
    u_h.rename("u","u")
    file_uh << u_h
    file_uexact << u_exact_func
    z_h.rename("z","z")
    file_zh << z_h
    file_boundaries  << boundaries
#    ez_dg_proj.rename("ez","ez")
#    file_ez << ez_dg_proj
    file_r_K << R_ez_K
    indicator_file << indicators
    refinement_marker_file << markers
    mesh_file << mesh

print "N_dof=", N_array
print "error_estimate=", eta_h

ones = np.ones(N_refinements)

import matplotlib.pyplot as plt
N_marie_ar = np.array([111,143,225,426,766,1458,2701,4989,9302,16985,31828,56741,105477])
M_u_h = np.array([-0.67924, -0.672806, -0.669766, -0.668054, -0.667466, -0.667052, -0.666876,-0.666782,-0.666727,-0.666701,-0.666685,-0.666677,-0.666672])
error_estimate=np.array([0.0110495,0.00571803,0.00293349,0.00135355,0.000784865,0.000378102,0.000207093,0.000114852,5.99159e-05,3.40065e-05,1.811e-05,1.0147e-05,5.54607e-06])

#M_u_h[:] = [x - 2/3.0 for x in M_u_h]
M_u_h = abs(-2/3.0 - M_u_h)
eta_h_marie =error_estimate/M_u_h
print eta_h_marie
plt.semilogx(N_array, efficiency_array, 'b', marker='o')
plt.semilogx(N_array, ones, 'b', marker='o')
plt.semilogx(N_marie_ar, eta_h_marie, 'r', marker='^')
plt.xlabel('N_dof')
plt.ylabel('efficiency')
plt.ylim((0.5,1.5))
plt.savefig('efficiency_DG2.png')
plt.show()



