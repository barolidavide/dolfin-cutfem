from dolfin import *
from mshr import *
import numpy as np

N = 10
gdim = 2

#set_log_level(DBG)

parameters["refinement_algorithm"] = "plaza_with_parent_facets"

#Output files
file_ez = File("./results/ez_dg.pvd")
file_r_K = File("./results/R_ez_K.pvd")
mesh_file = File("./results/mesh.pvd")
file_uh = File("./results/u_h.pvd")
file_uexact = File("./results/u_exact.pvd")
file_zh = File("./results/z_h.pvd")
file_boundaries = File("./results/boundaries.pvd")
refinement_marker_file = File("./results/refinement_marker.pvd");
indicator_file = File("./results/indicators.pvd");
file_marker = File("./results/marker.pvd")
file_alpha = File("./results/alpha.pvd")

rectangle = Rectangle(Point(-1.0,-1.0),Point(1.0,1.0))
#small_rectangle = Rectangle(Point(-1.0,-1.0,0),Point(0.0,0.0,-0.3))
segments = 10
circle = Circle(Point(0.0,0.0),0.5,segments)

domain = rectangle
domain.set_subdomain(1,circle)

# Create mesh
mesh = generate_mesh(domain, N)

plot(mesh)

cell_markers = MeshFunction('size_t', mesh, gdim, mesh.domains())


file_marker << cell_markers

# Define magnetic permeability
class Diffusivity(Expression):
    def __init__(self, mesh, markers,**kwargs):
        self.markers = markers
    def eval_cell(self, values, x, cell):
        if self.markers[cell.index] == 0:
            values[0] = 10 # alpha_1
        elif self.markers[cell.index] == 1:
            values[0] = 1     # alpha_2
    def update(self,mesh,markers):
        self.markers = markers

class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


dirchlet_bc = DirichletBoundary()
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
dirchlet_bc.mark(boundaries, 0)

# ---------------------------------------------------------------------- #
# Create facet marker for facets between particles and background domain #
# ---------------------------------------------------------------------- #
for facet in facets(mesh): # For all edges in the mesh
    num_f_cells = facet.num_entities(gdim)
    
    if(num_f_cells ==2 ): #interior edge
        cell_index_0 = int(facet.entities(gdim)[0])
        cell_index_1 = int(facet.entities(gdim)[1])
        marker0 =  cell_markers[cell_index_0]
        marker1 =  cell_markers[cell_index_1]
        
        if((marker0 == 1 and marker1 == 0)or(marker0 == 0 and marker1 == 1)):
            boundaries[facet.index()]=2

# Define a circular hole snap is to refine boundary of circle
center = Point(0, 0)
radius = 0.5
class Hole(SubDomain):
    
    def inside(self, x, on_boundary):
        r = sqrt((x[0] - center[0])**2 + (x[1] - center[0])**2)
        return r < 1.5*radius # slightly larger
    
    def snap(self, x):
        r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
        if r < 1.5*radius:
            x[0] = center[0] + (radius / r)*(x[0] - center[0])
            x[1] = center[1] + (radius / r)*(x[1] - center[1])

# Mark hole and extract submesh
hole = Hole()

#f = Expression("-2.0*(x[0]-1)")
#g = Expression(("pow((x[1]-1),2.0)","2.0*(x[0]-1.0)*(x[1]-1.0)"),degree=2)
#u_exact =  Expression("(x[0]-1.0)*pow((x[1]-1),2.0)",degree=2)

#Body force
f = Expression("pow((x[0]-1),2)",degree=2) #Expression("(x[0]-1)")
alpha = Diffusivity(mesh, cell_markers)

DG0 = FunctionSpace(mesh,"DG",0)
alpha_func = Function(DG0)
alpha_func.interpolate(alpha)

file_alpha << alpha_func

N_refinements = 10
N_array = np.zeros(N_refinements)
efficiency_array = np.zeros(N_refinements)
eta_ex = np.zeros(N_refinements)
eta_h = np.zeros(N_refinements)

gamma = 100


#TODO: replace the loop by using the adapt function for forms, this might make the code much faster
def a(u,v,alpha,n,h):
    a = alpha*inner(grad(u),grad(v))*dx
    a -= alpha*inner(dot(grad(u),n), v)*ds(0)
    a -= alpha*inner(dot(grad(v),n), u)*ds(0)
    a += gamma*alpha/h*inner(u, v)*ds(0)
    return a
    
def l(v):
    L = f*v*dx
    return L

def harmonic_weight(alpha):
    return (alpha('-')*alpha('+'))/(alpha('-')+alpha('+'))

def Q(u):
    #Q = 0.5*(dot(grad(u('-')),n('+'))+dot(grad(u('+')),n('+')))*dS(2)
    Q =  u*dx(1)
    return Q

for i in range(0, N_refinements):
    
    mesh_file << mesh

    n = FacetNormal(mesh)
    h = 2*Circumradius(mesh)

    #vertical_neumann_boundary.mark(boundaries,3)
    #patch_maker = PatchMaker(mesh)

    #Define measures with marked boundary
    ds = Measure('ds', domain = mesh, subdomain_data = boundaries)
    dS = Measure('dS', domain = mesh, subdomain_data = boundaries)
    dx = Measure('dx', domain=mesh, subdomain_data=cell_markers)

    #################################################
    ## Step 0: Solve primal problem on coarse mesh ##
    #################################################
    V = FunctionSpace(mesh,"CG",1)

    u = TrialFunction(V)
    v = TestFunction(V)

    a_primal = a(u,v,alpha,n,h)
    L_primal = l(v)

#    u0 = Constant(0.0)
#    bc = DirichletBC(V, u0,dirchlet_bc)

    u_h = Function(V)

    solve(a_primal == L_primal, u_h)
    
    #################################################
    ## Step 0: Solve reference primal ##
    #################################################
    V_2 = FunctionSpace(mesh,"CG",2)

    u_2 = TrialFunction(V_2)
    v_2 = TestFunction(V_2)

    a_primal_2 = a(u_2,v_2,alpha,n,h)
    L_primal_2 = l(v_2)
    
#    bc_2 = DirichletBC(V_2, u0,dirchlet_bc)

    u_h_2 = Function(V_2)
    
    solve(a_primal_2 == L_primal_2, u_h_2)


    #################################################
    ## Step 1: Solve dual problem on coarse mesh   ##
    #################################################
    a_dual = a(v,u,alpha,n,h) #adjoint(a_primal)
    L_dual = Q(v)

    z_h = Function(V)

    solve(a_dual == L_dual, z_h)

    #############################################################################################
    ## Step 2: Solve error equation approximately using a restricted DG2 space                 ##
    #############################################################################################
    V_dg2 = FunctionSpace(mesh,"DG",2)
    ez = TrialFunction(V_dg2)
    v_dg = TestFunction(V_dg2)


    a_e = a(v_dg,ez,alpha,n,h)
#    a_e -= alpha*inner(dot(grad(ez),n), v_dg)*ds(0)
#    a_e -= alpha*inner(dot(grad(v_dg),n), ez)*ds(0)
#    a_e += gamma*alpha/h*inner(ez, v_dg)*ds(0)

    L_e = Q(v_dg)-a(v_dg,z_h,alpha,n,h)
    L_e += inner(harmonic_weight(alpha)*(dot(grad(z_h('-')),n('+'))+dot(grad(z_h('+')),n('+'))),jump(v_dg))*dS #dS(0) #harmonic_weight(alpha)
    #L_e += inner(dot(grad(z_h),n),v_dg)*ds

    A_e = assemble(a_e)
    b_e = assemble(L_e)

    patch_ec = PatchErrorControl()
    patch_ec.restrict_vertices_DG2(A_e,b_e,V_dg2)

    ez = Function(V_dg2)
    solve(A_e,ez.vector(),b_e);

    #############################################################################################
    ## Step 3: Compute the dual weighted Residual                                              ##
    #############################################################################################
    V_dg0 = FunctionSpace(mesh,"DG",0)
    R_ez_K = Function(V_dg0)

    L_r  = l(ez) - a(u_h,ez,alpha,n,h)
    L_r += inner(harmonic_weight(alpha)*(dot(grad(u_h('-')),n('+'))+dot(grad(u_h('+')),n('+'))),ez('+'))*dS
    #L_r += inner(dot(grad(u_h),n),ez)*ds #harmonic_weight(alpha)

    L_r = Form(L_r)
    patch_ec.compute_R_ez_T(L_r, R_ez_K)

    sum_eta_T = R_ez_K.vector().sum()

    #############################################################################################
    ## Step 3: Compute indicators and markers and refine mesh                                  ##
    #############################################################################################
    indicators = CellFunction("double",mesh);
    patch_ec.compute_indicators_DG0(indicators,R_ez_K);
        
    markers = CellFunction("bool",mesh)
    strategy = "dorfler";
    fraction = 0.5;
    mark(markers, indicators, strategy, fraction)

    mesh = adapt(mesh,markers)
    cell_markers_old = cell_markers
    #cell_markers = adapt(cell_markers,mesh.leaf_node())
    #mesh.clear_child()
    
    cell_markers = MeshFunction('size_t', mesh, gdim, mesh.domains())
    
    if (mesh.data().exists("parent_cell", gdim)):
        parent = (mesh.data().array("parent_cell", gdim))
        size_p = len(parent)
        print size_p
        for k in range(0, size_p):
            parent_index = int(parent[k])
            cell_markers[k] = cell_markers_old[parent_index]

    file_boundaries  << boundaries

    #Boundary adaptation buggy
    #boundaries = adapt(boundaries,mesh)
    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    dirchlet_bc.mark(boundaries, 0)

    # ---------------------------------------------------------------------- #
    # Create facet marker for facets between particles and background domain #
    # ---------------------------------------------------------------------- #
    for facet in facets(mesh): # For all edges in the mesh
        num_f_cells = facet.num_entities(gdim)
        
        if(num_f_cells ==2 ): #interior edge
            cell_index_0 = int(facet.entities(gdim)[0])
            cell_index_1 = int(facet.entities(gdim)[1])
            marker0 =  cell_markers[cell_index_0]
            marker1 =  cell_markers[cell_index_1]
            
            if((marker0 == 1 and marker1 == 0)or(marker0 == 0 and marker1 == 1)):
                boundaries[facet.index()]=2

    alpha.update(mesh,cell_markers)

    # Snap boundary
    mesh.snap_boundary_marker(hole,boundaries,2)

    #####################################################################
    ##  Output results                                                 ##
    #####################################################################
    Q_uh = assemble(Q(u_h))
    print "Q=", Q_uh

    Q_ref = assemble(Q(u_h_2))
    print "Q_ref=", Q_ref
    
    N_array[i] = V.dofmap().global_dimension()
    eta_ex[i] = Q_ref-Q_uh #0.730406293988 #1.95007356541
    eta_h[i] = sum_eta_T
    efficiency_array[i] = sum_eta_T/eta_ex[i]

    print "N=", N_array[i]
    print "eta_exact=", eta_ex[i]
    print "sum_eta_T=",  eta_h[i]
    print "efficiency=", efficiency_array[i]

#    N_ref = 1
#    refined_mesh = refine(mesh)
#    for j in range(0, N_ref):
#        refined_mesh = refine(refined_mesh)
#
#    V_f_proj = FunctionSpace(refined_mesh,"CG",1)
#    ez_dg_proj = Function(V_f_proj)
#    ez_dg_proj.interpolate(ez)


    # Save to file and plot
    u_h.rename("u","u")
    file_uh << u_h
    # file_uexact << u_exact_func
    z_h.rename("z","z")
    file_zh << z_h

#    ez_dg_proj.rename("ez","ez")
#    file_ez << ez_dg_proj
    file_r_K << R_ez_K
    indicator_file << indicators
    refinement_marker_file << markers
    file_marker << cell_markers
    mesh_file << mesh



print "N_array=", repr(N_array)
print "eta_h=", repr(eta_h)
print "efficiency_array=", repr(efficiency_array)

#ones = np.ones(N_refinements)
#
#import matplotlib
#import matplotlib.pyplot
#matplotlib.use('TkAgg')
#matplotlib.pyplot.use('TkAgg')
#
#matplotlib.pyplot.semilogx(N_array, efficiency_array, 'b', marker='o')
##plt.semilogx(N_array, ones, 'b', marker='o')
#matplotlib.pyplot.xlabel('N_dof')
#plt.ylabel('efficiency')
##plt.ylim((0.5,1.5))
#plt.savefig('efficiency_DG2.png')
#plt.show()



